<?php global $wpo_wcpdf; ?>
<table class="head container">
	<tr>
		<td class="header">
		<?php
		if( $wpo_wcpdf->get_header_logo_id() ) {
			$wpo_wcpdf->header_logo();
		} else {
			echo apply_filters( 'wpo_wcpdf_invoice_title', __( 'Invoice', 'wpo_wcpdf' ) );
		}
		?>
		</td>
		<td>
			
		</td>
	</tr>
	<tr>
		<td>
			<h3 class="document-type-label">
			<?php if( $wpo_wcpdf->get_header_logo_id() ) echo apply_filters( 'wpo_wcpdf_invoice_title', __( 'Your iLectronics Order', 'wpo_wcpdf' ) ); ?>
			</h3>
			<?php do_action( 'wpo_wcpdf_after_document_label', 'invoice' ); ?>
		</td>
		<td>&nbsp;</td>
	</tr>

	<tr>
		<td>
			<div class="order-information">
			<?php if ( isset($wpo_wcpdf->settings->template_settings['display_number']) && $wpo_wcpdf->settings->template_settings['display_number'] == 'invoice_number') { ?>
				<span class="order-number-label"><?php _e( 'Invoice Number:', 'wpo_wcpdf' ); ?></span>
				<span class="order-number"><?php $wpo_wcpdf->invoice_number(); ?></span><br />
			<?php } ?>
			<?php if ( isset($wpo_wcpdf->settings->template_settings['display_date']) && $wpo_wcpdf->settings->template_settings['display_date'] == 'invoice_date') { ?>
				<span class="order-date-label"><?php _e( 'Invoice Date:', 'wpo_wcpdf' ); ?></span>
				<span class="order-date"><?php $wpo_wcpdf->invoice_date(); ?></span><br />
			<?php } ?>
				<span class="order-number-label"><?php _e( 'Order Number:', 'wpo_wcpdf' ); ?></span>
				<span class="order-number"><?php $wpo_wcpdf->order_number(); ?></span><br />
				<span class="order-date-label"><?php _e( 'Order Date:', 'wpo_wcpdf' ); ?></span>
				<span class="order-date"><?php $wpo_wcpdf->order_date(); ?></span><br />
				<span class="order-payment-label"><?php _e( 'Payment Method:', 'wpo_wcpdf' ); ?></span>
				<span class="order-payment"><?php $wpo_wcpdf->payment_method(); ?></span><br />
			</div>
		</td>
		<td>
			<div class="recipient-address"><?php $wpo_wcpdf->billing_address(); ?></div>
		</td>
	</tr>
</table><!-- head container -->

<?php do_action( 'wpo_wcpdf_before_order_details', 'invoice' ); ?>

<table class="order-details">
	<thead>
		<tr>
			<th class="product-label"><?php _e('Product', 'wpo_wcpdf'); ?></th>
			<th class="quantity-label"><?php _e('Quantity', 'wpo_wcpdf'); ?></th>
			<th class="price-label"><?php _e('Price', 'wpo_wcpdf'); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php $items = $wpo_wcpdf->get_order_items(); if( sizeof( $items ) > 0 ) : foreach( $items as $item ) : ?>
		<tr>
			<td class="description">
				<?php $description_label = __( 'Description', 'wpo_wcpdf' ); // registering alternate label translation ?>
				<span class="item-name"><?php echo $item['name']; ?></span><span class="item-meta"><?php echo $item['meta']; ?></span>
				<dl class="meta">
					<?php $description_label = __( 'SKU', 'wpo_wcpdf' ); // registering alternate label translation ?>
					<?php if( !empty( $item['sku'] ) ) : ?><dt><?php _e( 'SKU:', 'wpo_wcpdf' ); ?></dt><dd><?php echo $item['sku']; ?></dd><?php endif; ?>
					<?php if( !empty( $item['weight'] ) ) : ?><dt><?php _e( 'Weight:', 'wpo_wcpdf' ); ?></dt><dd><?php echo $item['weight']; ?><?php echo get_option('woocommerce_weight_unit'); ?></dd><?php endif; ?>
				</dl>
			</td>
			<td class="quantity"><?php echo $item['quantity']; ?></td>
			<td class="price"><?php echo $item['order_price']; ?></td>
		</tr>
		<?php endforeach; endif; ?>
	</tbody>
	<tfoot>
		<tr class="no-borders">
			<td class="no-borders" colspan="3">
				<table class="totals">
					<tfoot>
						
						<tr>
							<td class="no-borders">&nbsp;</td>
							<th class="description"><strong>Order Total</strong></th>
							<td class="price"><span class="totals-price"><?php
$woocommerce_totals = $wpo_wcpdf->get_woocommerce_totals();
echo $woocommerce_totals['order_total']['value'];
?></span></td>
						</tr>
						
						</tfoot>
				</table>
			</td>

		</tr>
	</tfoot>
</table><!-- order-details -->


<div class="instructions">
<table width="90%">
	<tr>
		<td>
			<h4>Thank you for choosing iLectronics. You are nearly there with your sale but please read the below carefully:</h4>
			<p><strong>Next Steps?</strong></p>
			<p>1 - Pop your device(s) in the supplied envelope - preferably in the original box if you have it but don’t worry if you don’t. We don’t need any accessories such as cables, headphones or chargers so please keep these. If everything does not fit you can use other packaging but please make sure that your order number is on the outside.</p>
			<p>2 - When we receive your device(s) we will let you know. We will check they match the description you selected and our terms; and will make your payment usually within 24 hours. The price you see is the price you get. As long as your device matches the description you selected we won't try to lower the price with hidden terms and conditions.</p>
			<p>3 - The legal bit - by sending your device(s) you agree to accept our terms and conditions on our website.</p>
			<p>4 - Any questions? - please email us at <a href="mailto:customerservice@ilectronics.co.uk">customerservice@ilectronics.co.uk</a> and we will get back to within 24 working hours.</p>
		</td>
	</tr>
</table>
</div>
<div class="packing-label">
<table width="100%">
	<tr>
		<td width="60%">
		<table width="100%">
			<tr>
				<td width="60%"><p><span class="order-number-label"><?php _e( 'Order Number:', 'wpo_wcpdf' ); ?></span>
				<span class="order-number"><?php $wpo_wcpdf->order_number(); ?></span><br /></p>
				</td>
				<td width="30%"><img align="center" src="http://ilectronics.wpengine.com/wp-content/uploads/2015/01/postage-stamp.png"</img>
				</td>
				<td width="10%">&nbsp;</td>
			</tr>
			<tr>
				<td width="60%">
					<p>Freepost RTJR-RYLS-EUSL<br />
					iLectronics Ltd<br />
					PO Box 3959<br />
					LEAMINGTON SPA<br />
					CV31 9AH</p>
				</td>
				<td width="30%">&nbsp;
				</td>
			</tr>
		</table>
		</td>
		<td width="40%">
		<table width="100%">
			<tr>
				<td width="80%"><p><span class="their-address"><?php $wpo_wcpdf->shipping_address(); ?></span></p>
				</td>
				<td width="20%">
				</td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td width="80%"><p><span class="if-undelivered">If undelivered please return to: <br />PO BOX 3959, Leamington Spa, CV31 9AH</span></p>
				</td>
				<td width="20%">&nbsp;
				</td>
			</tr>
		</table>
		</td>
		

</div>


<?php if ( $wpo_wcpdf->get_footer() ): ?>
<div id="footer">
	<?php $wpo_wcpdf->footer(); ?>
</div><!-- #letter-footer -->
<?php endif; ?>