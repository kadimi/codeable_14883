<?php
/*
Plugin Name: iLectronics
Plugin URI: http://www.kadimi.com/en/quote
Description: Miscellaneous enhancements
Version: 1.0.0
Author: Nabil Kadimi
Author URI: http://kadimi.com
License: GPL2
*/

// Include Skelet (written by Nabil Kadimi)
include dirname( __FILE__ ) . '/skelet/skelet.php';

// Include options definitions
skelet_dir( dirname( __FILE__ ) . '/data' );

// define fields to hide for each payment option
$GLOBALS['ie_payment_to_fields'] = array(
	'payment_method_bacs' => array(
		'cheque_name',
		'paypal_email',
	),
	'payment_method_cheque' => array(
		'bank_account',
		'paypal_email',
		'sort_code',
	),
	'payment_method_cod' => array(
		'cheque_name',
		'bank_account',
		'sort_code',
	),
);

// Dummy value to pass bypass validation for fields that we don't care about 
$GLOBALS['ie_dummy'] = 'no@none.no';

/**
 * Add something before the header
 *
 * This will be added after woocommerce_result_count & woocommerce_catalog_ordering
 */
add_action( 'woocommerce_before_shop_loop', 'ilectronics_product_archive_header', 40 );
function ilectronics_product_archive_header() {
	$product_archive_header = paf( 'ilectronics_product_archive_header' );
  	printf( '<div class="clear"><img src="%s" /></div>', $product_archive_header );
}

/**
 * Delete meta keys that have the dummy value
 */
add_action( 'save_post', 'ilectronics_save_order', 10, 2);
function ilectronics_save_order($post_id, $post){
	if ('shop_order' !== $post->post_type) {
		return;
	}

	$meta_keys = array();
	foreach ($GLOBALS['ie_payment_to_fields'] as $k=>$v) {
		$meta_keys = array_merge( $meta_keys, $v);
	}
	$meta_keys = array_values(array_unique($meta_keys));
	foreach ($meta_keys as $m) {
		delete_post_meta($post_id, $m, $GLOBALS['ie_dummy']);
	}
}

/**
 * The JavaScript for showing/hiding custom fields
 */
add_action( 'woocommerce_after_checkout_billing_form', 'ilectronics_checkout_fields' );
function ilectronics_checkout_fields() {
	?>
		<script>

			var ie_payment_to_fields = <?php echo json_encode($GLOBALS['ie_payment_to_fields']); ?>;
			var dummy = '<?php echo $GLOBALS['ie_dummy']; ?>';
			var ie_interval = false;
			var ie_intervals_num = 0;
			var ie_t = 500;
			var ie_n = 1;
			var ie_current_payment_method = 'foo';
			var ie_previous_payment_method = 'bar';
			var ie_custom_fields = [];
			for (i in ie_payment_to_fields) {
				for (j in ie_payment_to_fields[i]) {
					ie_custom_fields.push(ie_payment_to_fields[i][j]);
				}
			}
			ie_custom_fields = ie_custom_fields.reduce(function(p, c){if(p.indexOf(c)<0) p.push(c); return p;},[]);

			jQuery( document ).ready( function($) {

				ie_bind();

				$(document).on('updated_checkout', 'body', ie_bind);
				// Repeat to make sure binding happens even after WC clears it
				// Start another N seconds binding loop when the payment method changes
				setInterval(function() {
					ie_previous_payment_method = ie_current_payment_method;
					ie_current_payment_method = $('[name=payment_method]:checked').val();
					if ( ie_previous_payment_method !== ie_current_payment_method ) {
						ie_intervals_num = 0;
					}
					if (ie_intervals_num < ie_n) {
						ie_intervals_num++;
						ie_bind();
					}
				}, ie_t);

				// Hide fields at first
				for (var i in ie_custom_fields){
					$c = $('#' + ie_custom_fields[i] + '_field').wrap('<div style="display:none"></div>');
				}



			} );

			// Bind handlers to payment options radio buttons
			function ie_bind() {

				var $ = jQuery;
				var $c;

				// Force the click to happen
				jQuery('[name=payment_method]:checked').click();

				// Synchronize
				if (!$('#ie_custom_fields_clone', '#payment').length) {
					$( '<div id="ie_custom_fields_clone" style="display:none"><h3><?php _e('Payment Information'); ?></h3></div>' ).insertAfter( ".payment_methods" );
					for (i in ie_custom_fields){
						$c = $('#' + ie_custom_fields[i] + '_field').clone();
						$c
							.css('width', 'auto')
							.css('float', 'none')
						;
						$c.find('input').css('margin', '0');
						$( "#ie_custom_fields_clone" ).append( $c );
						$( "#ie_custom_fields_clone" ).slideDown('fast');						
					}
					$(document).on(
						'keyup blur change',
						'#ie_custom_fields_clone input[type=text]',
						function() {
						var $this = $(this);
						var input_id = $this.attr('id');
						var val = $this.val();
						$('input[id='+input_id+']').val( val );
					})
				}

				// Bind showing handler
				$('[name=payment_method]').on('click change', function() {
					var $p = $(this);
					var p_id = $p.attr('id');
					var p_fields = [];
					var p_fields_selector = '';
					
					// Selector of payment
					if (typeof ie_payment_to_fields[p_id] != 'undefined') {
						p_fields = ie_payment_to_fields[p_id];
						p_fields_selector = '#'+p_fields.join('_field,#')+'_field'
					}

					$('[data-ie-custom]')
						.not(p_fields_selector)
						.filter(':hidden')
						.filter(function() {
							var $this = $(this);
							var $p = $('p[id=' + $this.attr('id') + ']');
							var $input = $('input',$p);

							$p.removeClass( 'woocommerce-invalid woocommerce-invalid-email');
							$input.val('');

							$p.slideDown('fast')
							return true;
						})
					;
				});

				// Bind hiding handler
				for (p in ie_payment_to_fields) {
					$( '#' + p )
						.unbind('click',ie_hide_cf)
						.click(ie_hide_cf)
					;
				};
			}

			// High custom order fields that should be hidden
			function ie_hide_cf() {
				var $ = jQuery;
				var $p = $(this);
				var p_id = $p.attr('id');
				var p_fields = ie_payment_to_fields[p_id];

				for(f in p_fields) {
					$('#{{f}}_field'.replace('{{f}}', p_fields[f])).hide(0, function(){
						var $this = $(this);
						var $p = $('p[id=' + $this.attr('id') + ']');
						var $input = $('input',$p);
						$p.hide();
						$input.val(dummy);
					});
					$('#{{f}}_field'.replace('{{f}}',p_fields[f])).attr('data-ie-custom', '1');
				}
			}
		</script>
	<?php 
}
